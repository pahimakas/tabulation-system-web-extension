define(["app.module", "param"], function (main, param) {
    main.requires.push('ngMessages');
    main.requires.push('ngMaterial');
    main.controller("LoginCtrl", ["$scope", "$http", "$q", "toast", "Event_ID", login]);
    main.value('Event_ID', {
        'event_id': ''
    });

    function login(scope, http, q, toast, evid) {
        // Centralized Handling of HTTP Request
        function httpHandler(form_data, url_path) {
            var wait = q.defer();
            http({
                method: 'POST',
                url: url_path,
                data: param(form_data),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (r) {
                wait.resolve({
                    'response': r
                })
            });
            return wait.promise;
        }

        // Event Select
        scope.type = 'e';
        // Load Events
        scope.loadEvents = function () {
            http.get('/process/components/data.php?provider=events').then(function (r) {
                switch (r.status) {
                    case 202:
                        scope.event_list = r.data;
                        break;
                    default:
                        console.log('Unknown Response')
                }
            })
        };
        // Centralized Submission Function
        scope.submit = function (form) {
            switch (scope.type) {
                case 'l':
                    form.username.$setValidity('wrongUsername', true);
                    form.password.$setValidity('wrongPassword', true);
                    // Login User Action
                    var form_obj = {
                        'action': 'login',
                        'event_id': evid.event_id.event_id,
                        'username': form.username.$modelValue,
                        'password': form.password.$modelValue
                    };
                    var url = 'process/shared/auth.php?';
                    httpHandler(form_obj, url).then(function (r) {
                        var response = r.response;
                        switch (response.status) {
                            case 202:
                                localStorage.setItem('username', response.data.shake.username);
                                localStorage.setItem('hash', response.data.shake.hash);
                                toast.SimpleToast('Successfully Login');
                                scope.$emit('success-login');
                                break;
                            case 201:
                                toast.SimpleToast('User is Valid, but not allowed on selected event.');
                                break;
                            case 203:
                                // User information error
                                switch (response.data.message) {
                                    case 'username':
                                        form.username.$setValidity('wrongUsername', false);
                                        break;
                                    case 'password':
                                        form.password.$setValidity('wrongPassword', false);
                                        break;
                                    default:
                                        toast.SimpleToast('User is not Found');
                                }
                                break;
                            default:
                                console.log('Bal2x ng response...');
                        }
                    });
                    break;
                case 'v':
                    form.pin.$setValidity('wrongPin', true);
                    // Create new user verification
                    var form_obj = {
                        'admin_request': true,
                        'pin': form.pin.$modelValue
                    };
                    var url = 'process/components/verify.php';
                    httpHandler(form_obj, url).then(function (r) {
                        var response = r.response;
                        console.log(response);
                        switch (response.status) {
                            case 202:
                                scope.type = 'c';
                                break;
                            case 203:
                                form.pin.$setValidity('wrongPin', false);
                                break;
                            default:
                                console.log('Bal2x ng response...');
                        }
                    });
                    break;
                case 'c':
                    console.log(form);
                    // Create user Action
                    var form_obj = {
                        'create_user': true,
                        'event_id': evid.event_id.event_id,
                        'username': form.username_new.$modelValue,
                        'password': form.password_new.$modelValue,
                        'Name': form.Name_new.$modelValue,
                    };
                    var url = 'process/components/create_user.php';
                    httpHandler(form_obj, url).then(function (r) {
                        var response = r.response;
                        switch (response.status) {
                            case 202:
                                scope.type = 'l';
                                toast.SimpleToast('User Account Created');
                                break;
                            case 203:
                                toast.SimpleToast('User Account not Created');
                                break;
                            default:
                                console.log('Bal2x ng response...');
                        }
                    });
                    break;
                default:
                    console.log('Opps..');
            }
        };
        // Check Username Availability
        scope.check_availability = function (form) {
            http.get('/process/components/verify.php?check_availability=' + form.username_new.$modelValue).then(function (r) {
                switch (r.status) {
                    case 200:
                        form.username_new.$setValidity('existUsername', true);
                        break;
                    case 203:
                        form.username_new.$setValidity('existUsername', false);
                        break;
                    default:
                        console.log('Unknown Response');
                }
            })
        };
        scope.updateEvent = function (eve) {
            evid.event_id = eve;
            scope.activate = evid.event_id.event_id > 0;
        };
        scope.activate = evid.event_id.event_id > 0;
    }
});
