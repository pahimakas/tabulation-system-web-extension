define(["app.module"], function (main) {
    main.requires.push('ngMaterial');
    main.controller('NavigationCtrl', ["$scope", "$mdSidenav", nav]);

    function nav(scope, mdSidenav) {
        scope.toggleLeft = buildToggler('left');

        function buildToggler(componentId) {
            return function () {
                mdSidenav(componentId).toggle();
            };
        }

    }
});
