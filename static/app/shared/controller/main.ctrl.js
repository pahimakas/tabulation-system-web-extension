define(["app.module", "param", "loadCSS", "onloadCSS", "LoginCtrl"], function (main, param) {
    main.requires.push('ngAnimate');
    main.requires.push('ngMaterial');

    // Authentication Temporary Variable
    main.value('USER_AUTHENTICATION', {
        is_login: false
    });

    // Pageant Event Temporary Variable
    main.value('PAGEANT_EVENT', {
        selected: '',
    });

    // Module for zooming candidate photo
    main.directive('zoom', function ($window) {
        function link(scope, element, attrs) {
            var frame, image, zoomlvl, fWidth, fHeight, rect, rootDoc, offsetL, offsetT, xPosition, yPosition, pan;
            scope.$watch('$viewContentLoaded', function () {
                frame = angular.element(document.querySelector("#" + scope.frame))[0];
                image = angular.element(document.querySelector("#" + scope.img))[0];
                zoomlvl = (scope.zoomlvl === undefined) ? "2.5" : scope.zoomlvl
            });
            scope.trackMouse = function ($event) {
                fWidth = frame.clientWidth;
                fHeight = frame.clientHeight;
                rect = frame.getBoundingClientRect();
                rootDoc = frame.ownerDocument.documentElement;
                offsetT = rect.top + $window.pageYOffset - rootDoc.clientTop;
                offsetL = rect.left + $window.pageXOffset - rootDoc.clientLeft;
                xPosition = (($event.pageX - offsetL) / fWidth) * 100;
                yPosition = (($event.pageY - offsetT) / fHeight) * 100;
                pan = xPosition + "% " + yPosition + "% 0";
                image.style.transformOrigin = pan;
            };
            element.on('mouseover', function (event) {
                image.style.transform = 'scale(' + zoomlvl + ')'
            });
            element.on('mouseout', function (event) {
                image.style.transform = 'scale(1)'
            })
        }

        return {
            restrict: 'EA',
            scope: {
                src: '@src',
                frame: '@frame',
                img: '@img',
                zoomlvl: '@zoomlvl'
            },
            template: ['<div id="{{ frame }}" class="zoomPanFrame" >', '<img id="{{ img }}" class="zoomPanImage md-card-image" ng-src= "{{ src }}" ng-mousemove="trackMouse($event)"></img>', '</div>'].join(''),
            link: link
        };
    });

    main.controller('MainCtrl', ['$timeout', '$scope', '$q', '$http', mainCtrl]);

    function mainCtrl(timeout, scope, q, http) {
        var val = 0;

        function trigger() {
            val = val + 1
        }

        // Lazy Loading of CSS
        function initiate() {
            function loadAllAssets() {
                scope.$evalAsync(function () {
                    onloadCSS(loadCSS("static/app/assets/css/libs/angular-material.min.css"), trigger);
                });
                scope.$evalAsync(function () {
                    onloadCSS(loadCSS("static/app/assets/css/libs/docs.css"), trigger);
                });
                scope.$evalAsync(function () {
                    onloadCSS(loadCSS("static/app/assets/iconfont/material-icons.css"), trigger);
                });
                scope.$evalAsync(function () {
                    onloadCSS(loadCSS("static/app/assets/css/modules/nav.css"), trigger);
                });
                scope.$evalAsync(function () {
                    onloadCSS(loadCSS("static/app/assets/css/modules/app.css"), trigger);
                });
                scope.$evalAsync(function () {
                    onloadCSS(loadCSS("static/app/assets/css/libs/loading-bar.min.css"), trigger);
                });
            }

            // Lazy loading companion
            function showUI() {
                scope.my_msg = ' ';
                timeout(function () {
                    if (val === 6) {
                        scope.my_msg = 'Launching...';
                        scope.$broadcast('FinishTalk');
                    } else {
                        timeout(function () {
                            showUI();
                        }, 1000);
                    }
                }, 500);
            }

            // Check Local Storage of Browser if first install
            if (localStorage.getItem('manifest') === null) {
                var pending = q.defer();

                function init() {
                    loadAllAssets();
                    var date = new Date();
                    var manifest = {
                        'AppName': 'Tabulation - Web Based Tabulation System',
                        'init': {
                            'date': date,
                            'browser': {
                                'AppVersion': navigator.AppVersion,
                                'lang': navigator.language,
                                'platform': navigator.platform,
                                'userAgent': navigator.userAgent
                            },
                        }
                    };
                    localStorage.setItem('manifest', JSON.stringify(manifest))
                }

                function getMsg() {
                    http.get('process/components/data.php?provider=welcome').then(function (response) {
                        if (response.status === 200) {
                            pending.resolve({
                                'message': response.data
                            });
                            init();
                        } else {
                            pending.resolve({
                                'message': ['Hi!', 'This is Pahimakas', 'A Web Based', 'Tabulation System', 'making tabulation', 'Faster', 'and Easier!']
                            });
                            init();
                        }
                    });
                    return pending.promise;
                }

                getMsg().then(function (result) {
                    var msg = result.message;

                    function presentMessage() {
                        timeout(function () {
                            if (msg.length > 0) {
                                scope.my_msg = ' ';
                                timeout(function () {
                                    scope.my_msg = ' ';
                                    scope.my_msg = msg.shift();
                                    presentMessage();
                                }, 500);
                            } else if (msg.length === 0) {
                                showUI();
                            }
                        }, 2000);
                    }

                    presentMessage();
                });
            } else {
                loadAllAssets();
                showUI();
            }
        }

        initiate();
    }


    // Navigation Wrapper decides what to paint on first web page load whether
    // - login page if not authenticated or
    // - dashboard page if user is authenticated
    // Directive name is equal to 'app-navigation-wrapper' which is also located at
    // the index.html body tag
    main.directive('appNavigationWrapper', ['$rootScope', '$compile', '$timeout', '$http', '$q', '$location', 'USER_AUTHENTICATION', 'PAGEANT_EVENT', anp]);

    function anp($rootScope, $compile, $timeout, $http, $q, $location, auth, pe) {
        // Load login asset
        var loginAsset = angular.element(document.querySelector('.login-asset-wrapper'));
        // Check if user is authenticated, verification url relative path : /process/components/verify.php
        var is_authenticated = function () {
            var wait = $q.defer();
            var form_data = {
                'integrity': true,
                'username': localStorage.getItem('username'),
                'hash': localStorage.getItem('hash')
            };
            $http({
                method: 'POST',
                url: '/process/components/verify.php',
                data: param(form_data),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (r) {
                switch (r.status) {
                    case 202:
                        wait.resolve({
                            'authenticated': true
                        });
                        break;
                    case 203:
                        wait.resolve({
                            'authenticated': false
                        });
                        break;
                }
            });
            return wait.promise;
        };
        // Get navigation page, navigation directory page : static/app/shared/navigation/nav.tmpl.html
        var getNavigation = function () {
            var loading = $q.defer();
            $http.get('static/app/shared/navigation/nav.tmpl.html').then(function (r) {
                if (r.status === 200) {
                    loading.resolve({
                        'html': r.data
                    })
                } else {
                    $location.reload();
                }
            });
            return loading.promise;
        };
        // Get navigation page, navigation directory page : static/app/authentication/login.tmpl.html'
        var getLoginPage = function () {
            var loading = $q.defer();
            $http.get('static/app/authentication/login.tmpl.html').then(function (r) {
                if (r.status === 200) {
                    loading.resolve({
                        'html': r.data
                    })
                } else {
                    $location.reload();
                }
            });
            return loading.promise;
        };
        // Build Navigation bar and Dashboard page it also remove the login page
        var buildAppShell = function (scope, elem) {
            getNavigation().then(function (r) {
                $timeout(function () {
                    scope.$evalAsync(function () {
                        elem.append($compile(r.html)(scope));
                    });
                    scope.$evalAsync(function () {
                        scope.isLoadingFade = true;
                    });
                    scope.$evalAsync(function () {
                        scope.hideLogin = true;
                    });
                    $timeout(function () {
                        loginAsset.remove();
                        scope.$evalAsync(function () {
                            scope.$evalAsync(function () {
                                scope.isLoadingRemove = true;
                            });
                            scope.$evalAsync(function () {
                                angular.element(document.querySelector('.loading-page')).remove();
                            });
                            scope.$evalAsync(function () {
                                angular.element(document.querySelector('.login-page')).remove();
                            });
                        });
                    }, 500);
                }, 1000);
            });
        };
        // Build Login Page and call Login Controller
        var buildLogin = function (scope, elem) {
            if (loginAsset[0].children.length < 1) {
                scope.$evalAsync(function () {
                    loginAsset.append("<link rel=\"stylesheet\" href=\"static/app/assets/css/modules/login.css\">");
                });
            }
            $rootScope.hideLogin = false;
            getLoginPage().then(function (r) {
                $timeout(function () {
                    scope.$evalAsync(function () {
                        elem.append($compile(r.html)(scope));
                    });
                    scope.$evalAsync(function () {
                        scope.isLoadingFade = true;
                    });
                    scope.$evalAsync(function () {
                        $rootScope.is_login_click = false;
                    });
                    $timeout(function () {
                        scope.$evalAsync(function () {
                            scope.isLoadingRemove = true;
                        });
                        scope.$evalAsync(function () {
                            angular.element(document.querySelector('.loading-page')).remove();
                        });
                    }, 1000);
                }, 2000);
            });
        };
        var n = {
            link: function (scope, elem) {
                // How to trigger
                // e.g. $scope.$emit('success-login');
                // this could be execute out of the function
                // When 'success-login' is trigger inner function executed
                scope.$on('success-login', function (d) {
                    if (d) {
                        buildAppShell(scope, elem);
                    }
                });
                // When 'success-logout' is trigger inner function executed
                scope.$on('success-logout', function () {
                    localStorage.removeItem('username');
                    localStorage.removeItem('hash');
                    location.reload();
                });
                // When 'FinishTalk' is trigger inner function executed
                // This is the lazy message on welcome
                scope.$on('FinishTalk', function () {
                    is_authenticated().then(function (r) {
                        if (r.authenticated) {
                            buildAppShell(scope, elem);
                        } else {
                            buildLogin(scope, elem)
                        }
                    });
                });
            }
        };
        return n;
    }

    // This is the message toast API
    // It could be reusable in any function
    // just include toast on controller
    // e.g. main.controller('SampleControllerHere', ['toast', function(toast){...});
    main.service("toast", ["$mdToast", toaster]);

    function toaster(mdt) {
        var sys = this;
        // How to use
        // toast.SimpleToast('Hi there!', 3000)
        sys.SimpleToast = function (message, duration) {
            mdt.show(
                mdt.simple()
                    .textContent(message)
                    .position('bottom right')
                    .hideDelay(duration || 3000)
            );
        };
        // How to use
        // toast.MessageToast(instance, 3000)
        // Has own controller 'MessageToastCtrl'
        // Custom template HTML 'static/app/shared/toast/toast.message.tmpl.html'
        sys.MessageToast = function (instance, duration) {
            mdt.show({
                locals: {
                    instance: instance
                },
                hideDelay: duration || 5000,
                position: 'bottom right',
                controller: 'MessageToastCtrl as mtc',
                templateUrl: 'static/app/shared/toast/toast.message.tmpl.html'
            });
        }
    }

    // Message Toast Controller
    main.controller('MessageToastCtrl', ["$scope", "$mdToast", "$location", "instance", mtc]);

    function mtc(mdt, loc, instance) {
        var mtc = this;
        mtc.message = instance.body;
        mtc.from = instance.placeholder.user;
        mtc.closeToast = function () {
            mdt.hide()
        };
        mtc.openMoreInfo = function () {
            loc.path(instance.action.path)
        };
    }


    // About Controller callable with mdDialog
    main.controller('AboutCtrl', ["$scope", "$mdDialog", AboutCtrl]);

    function AboutCtrl($scope, $mdDialog) {
        // Hide Dialog
        $scope.hide = function () {
            $mdDialog.hide();
        };
        // Cancel Dialog
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        // Answer Dialog
        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };
    }


    // Navigation Controller, companion of NavigationAppWrapper Directive
    main.controller('NavigationCtrl', ["$scope", "$mdSidenav", "$http", "$q", "$mdDialog", "toast", nav]);

    function nav(scope, mdSidenav, http, q, $mdDialog, toast) {
        var loading = q.defer();
        // Reusable Http Provider for getting user info, get user data, and logout
        var httpProvider = function (url_path, args, method) {
            http({
                method: method,
                url: url_path,
            }).then(function (r) {
                loading.resolve({
                    'response': r
                })
            });
            return loading.promise;
        };
        // Toggling mechanism of navigation bar
        scope.toggleLeft = buildToggler('left');
        // Get event information
        // Event info url relative path: '/process/components/data.php?provider=event_info'
        var getEventInfo = function () {
            var loading = q.defer();
            http.get('/process/components/data.php?provider=event_info').then(function (r) {
                switch (r.status) {
                    case 202:
                        loading.resolve({
                            'response': r
                        });
                        break;
                    case 203:
                        loading.reject({
                            'error': 'Something went wrong'
                        });
                        break;
                    default:
                        console.log('Unknown Response')
                }
            });
            return loading.promise;
        };
        // Real get Event Info with Promise injected
        getEventInfo().then(function (r) {
            scope.event_data_id = r.response.data.event_id;
            scope.event_data_name = r.response.data.name;
        });
        // Get user data information
        // User Data info path: '/process/components/data.php?provider=user_data'
        var init = function () {
            httpProvider('/process/components/data.php?provider=user_data',
                '', 'GET').then(function (r) {
                scope.username = r.response.data.username;
                scope.name = r.response.data.full_name;
            });
        };
        // Execute get user information
        init();
        // Get user logout
        // Logout action path dialog '/process/shared/auth.php?action=logout'
        // Trigger Confirmation Dialog on logout
        scope.logout = function (eve) {
            var confirm = $mdDialog.confirm()
                .title('Logout')
                .textContent('What would you like to logout?')
                .ariaLabel('Logout Confirmation')
                .targetEvent(eve)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                http.get('/process/shared/auth.php?action=logout').then(function (r) {
                    switch (r.status) {
                        case 202:
                            toast.SimpleToast('Successfully Logout');
                            scope.$emit('success-logout');
                            break;
                        default:
                            console.log('Opps..')
                    }
                })
            }, function () {
                console.log('No')
            });
        };
        // Show about dialog
        // Template relative path: '/static/app/components/templates/about.tmpl.html'
        // Has it own controller named: AboutCtrl
        scope.about = function (eve) {
            $mdDialog.show({
                controller: "AboutCtrl as ac",
                templateUrl: '/static/app/components/templates/about.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: eve,
                clickOutsideToClose: true,
                fullscreen: true
            });
        };

        // Navigation menu toggler helper
        function buildToggler(componentId) {
            return function () {
                mdSidenav(componentId).toggle();
            };
        }
    }

    // Dashboard Controller
    main.controller('DashboardCtrl', ["$scope", "$http", "$q", "$mdDialog", dashboardCtrl]);

    function dashboardCtrl(scope, http, q, $mdDialog) {
        var s = this;

        // Initiate and get exposure list
        // Exposure list relative path: '/process/components/data.php?provider=exposure'
        function init() {
            var loading = q.defer();
            http.get("/process/components/data.php?provider=exposure").then(function (r) {
                switch (r.status) {
                    case 202:
                        loading.resolve({
                            'data': r.data
                        });
                        break;
                    case 203:
                        loading.reject({
                            'error': r.data
                        });
                        break;
                    default:
                        console.log('Unknown Error');
                }
            });
            return loading.promise;
        }

        // Design exposure itme style
        s.item_style = function (url) {
            return {
                "background-image": "url(/media/exposure/" + url + ".jpg)",
                "background-size": " cover",
                "filter": "grayscale(1)",
                "color": "white",
            }
        };

        // Open Judging Dialog
        // Has it own controller named 'ExposureCtrl'
        // HTML Template relative url: '/static/app/components/templates/exposure.dialog.tmpl.html'
        s.open = function (data, eve) {
            $mdDialog.show({
                locals: ({
                    instance: data
                }),
                multiple: true,
                controller: "ExposureCtrl as ec",
                templateUrl: '/static/app/components/templates/exposure.dialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: eve,
                clickOutsideToClose: true,
                fullscreen: true
            });
        };

        init().then(function (r) {
            s.exposure_list = r.data;
        })
    }

    // Exposure Controller
    main.controller('ExposureCtrl', ["$scope", "$http", "$q", "$mdDialog", "instance", "toast", "$timeout", ExposureCtrl]);

    function ExposureCtrl(scope, http, q, mdDialog, instance, toast, timeout) {
        var ec = this;
        var temp;
        ec.title = instance.name;
        ec.exposure_id = instance.exposure_id;
        ec.cancel = function (ev) {
            if (ec.playbox.locked === false && ec.playbox.full_name !== undefined) {
                // Show confirmation on on un-save changes
                var confirm = mdDialog.confirm()
                    .title('Confirmation')
                    .multiple(true)
                    .textContent('You have not locked the score of ' + ec.playbox.full_name + '\n changes you made will not be submitted. Continue?')
                    .ariaLabel('Confirmation')
                    .targetEvent(ev)
                    .ok('Yes')
                    .cancel('No');
                mdDialog.show(confirm).then(function () {
                    mdDialog.cancel();
                }, function () {
                    toast.SimpleToast('Continue Scoring');
                });
            } else {
                mdDialog.cancel();
            }
        };
        ec.choose = function (l, ev) {
            temp = l;
            var m = ec.playbox === undefined ? 'Not Equal' : ec.playbox.full_name;
            if (m !== l.first_name + ' ' + l.last_name) {
                ec.playbox.wrap = true;
                ec.playbox = {};
                getCriteria(l).then(function (r) {
                    ec.playbox.wrap = false;
                    ec.playbox.is_scored = l.is_scored;
                    ec.playbox.criteria_list = r.response.data;
                    ec.playbox.candidate_number = l.candidate_number;
                    ec.playbox.full_name = l.first_name + ' ' + l.last_name;
                    ec.playbox.project_name = l.project_name;
                    ec.playbox.active = l.active;
                    ec.playbox.candidate_id = l.candidate_id;
                    ec.update(ec.playbox.criteria_list, l.is_scored);
                })
            } else {
                toast.SimpleToast('Already Selected')
            }
        };
        // Update function
        ec.update = function (lst, v) {
            var count = 0;
            var v1 = v || undefined;
            angular.forEach(lst, function (raw) {
                if (!isNaN(raw.data)) {
                    count = count + Number(raw.data);
                }
            });
            if (count !== 0 && v1) {
                ec.playbox.locked = true;
            } else if (count !== 0 && v1 === undefined) {
                ec.playbox.locked = false;
            } else {
                ec.playbox.locked = undefined;
            }
            ec.playbox.total = count;
        };
        scope.locked = function (con) {
            console.log(con);
            if (con.locked) {
                var form_data = {
                    'submission': 'submit_score',
                    'obj': con,
                    'exposure_id': ec.exposure_id,
                    'candidate_id': con.candidate_id
                };
                http({
                    method: 'POST',
                    url: '/process/components/data.php',
                    data: param(form_data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).then(function (p) {
                    switch (p.status) {
                        case 200:
                            var l = temp;
                            var count = 0;
                            getC();
                            l.is_scored = true;
                            ec.playbox.is_scored = true;
                            ec.playbox.criteria_list = p.response.data;
                            ec.playbox.candidate_number = l.candidate_number;
                            angular.forEach(ec.playbox.criteria_list, function (raw) {
                                console.log(raw.data);
                                if (!isNaN(raw.data)) {
                                    count = count + Number(raw.data);
                                }
                            });
                            console.log(count);
                            ec.playbox.total = count;
                            break;
                        default:
                            console.log('Not Saved')
                    }
                });
            }
        };
        // Get candidate user path
        // Relative base path: '/media/candidates/'
        // Note required photo format '.jpg' and Photo name
        // should CamelCase relative to to candidate full name
        ec.filter_name_path = function (name) {
            if (name != undefined) {
                var base_path = '/media/candidates/';
                return base_path + String(name).replace(/ /g, "") + '.jpg';
            }
        };
        // Get candidates data
        // Relative user path : '/process/components/data.php?provider=candidates'
        var getCandidates = function () {
            var loading = q.defer();
            http.get('/process/components/data.php?provider=candidates&exposure_id=' + instance.exposure_id).then(function (r) {
                switch (r.status) {
                    case 202:
                        loading.resolve({
                            'response': r
                        });
                        break;
                    case 203:
                        loading.reject({
                            'error': 'Something went wrong'
                        });
                        break;
                    default:
                        console.log('Unknown Response')
                }
            });
            return loading.promise;
        };
        // Get criteria data list
        // Relative action path : '/process/components/data.php?provider=criteria&exposure_id={exposure_id}'
        var getCriteria = function (l) {
            var loading = q.defer();
            if (l.is_scored) {
                http.get('/process/components/data.php?provider=criteria_mod&exposure_id=' + instance.exposure_id + '&candidate_id=' + l.candidate_id).then(function (r) {
                    switch (r.status) {
                        case 202:
                            loading.resolve({
                                'response': r
                            });
                            break;
                        case 203:
                            loading.reject({
                                'error': 'Something went wrong'
                            });
                            break;
                        default:
                            console.log('Unknown Response')
                    }
                });
            } else {
                http.get('/process/components/data.php?provider=criteria&exposure_id=' + instance.exposure_id).then(function (r) {
                    switch (r.status) {
                        case 202:
                            loading.resolve({
                                'response': r
                            });
                            break;
                        case 203:
                            loading.reject({
                                'error': 'Something went wrong'
                            });
                            break;
                        default:
                            console.log('Unknown Response')
                    }
                });
            }
            return loading.promise;
        };
        // Get candidates male and female list with
        // promise inject
        function getC() {
            getCandidates().then(function (r) {
                ec.male_list = [];
                ec.female_list = [];
                ec.male_list = r.response.data.male;
                ec.female_list = r.response.data.female;
            });
        }

        getC();
    }

    // Change global theme
    main.config(function ($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('purple')
            .accentPalette('deep-purple');
    });
});
