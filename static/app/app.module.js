'use strict';
// USAGE INFO:
// This is the core module of the web application which provides links into
// angularjs and non-angularjs libraries.

define(function () {
    // Declare angular module
    let main = angular.module("webapp", []);
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['webapp']);
    });
    return main;
});
