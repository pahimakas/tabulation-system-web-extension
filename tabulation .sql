-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2018 at 09:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tabulation`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `CandidateID`     int(10)    NOT NULL,
  `EventID`         int(10)    NOT NULL,
  `CandidateNumber` int(10)    NOT NULL,
  `Lastname`        text       NOT NULL,
  `Firstname`       text       NOT NULL,
  `Projectname`     text       NOT NULL,
  `Gender`          text       NOT NULL,
  `Active`          tinyint(1) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`CandidateID`, `EventID`, `CandidateNumber`, `Lastname`, `Firstname`, `Projectname`, `Gender`, `Active`)
VALUES
  (1, 4, 1, 'Gonzales', 'Ramil Joshua', 'Mr. CAS', 'Male', 1),
  (2, 4, 2, 'Saluntao', 'Jeff Neuman', 'Mr. CAS', 'Male', 1),
  (3, 4, 3, 'Tan', 'Joshua', 'Mr. COE', 'Male', 1),
  (4, 4, 4, 'Padla', 'Nathanael Jericho', 'Mr. COE', 'Male', 1),
  (5, 4, 5, 'Rojo', 'Jade Ryan', 'Mr. CSDT', 'Male', 1),
  (6, 4, 6, 'Gabonada', 'Tejie', 'Mr. CSDT', 'Male', 1),
  (7, 4, 7, 'Sandot', 'Alfrien', 'Mr. CON', 'Male', 1),
  (8, 4, 8, 'Julapong', 'Willineil', 'Mr. COB', 'Male', 1),
  (9, 4, 9, 'Bastasa', 'Alfred Jude', 'Mr. COB', 'Male', 1),
  (10, 4, 10, 'Balatero', 'Prynz jyn', 'Ms. CAS', 'Female', 1),
  (11, 4, 11, 'Torregoza', 'Sean Britney', 'Ms. CAS', 'Female', 1),
  (12, 4, 12, 'Barabat', 'Krizamae', 'Ms. COE', 'Female', 1),
  (13, 4, 13, 'Pama', 'Christine Diane', 'Ms. COE', 'Female', 1),
  (14, 4, 14, 'Vega', 'Jezza Nina Marie', 'Ms. CSDT', 'Female', 1),
  (15, 4, 15, 'Casinabe', 'Carla Mia', 'Ms. CSDT', 'Female', 1),
  (16, 4, 16, 'Pablero', 'Mary Chasty', 'Ms. CON', 'Female', 1),
  (17, 4, 17, 'Damasco', 'Jasvincelou Claudine', 'Ms. COB', 'Female', 1),
  (18, 4, 18, 'Cordova', 'Alyssa Marie', 'Ms. COB', 'Female', 1);

-- --------------------------------------------------------

--
-- Table structure for table `criteria`
--

CREATE TABLE `criteria` (
  `CriteriaID` int(10) NOT NULL,
  `ExposureID` int(10) NOT NULL,
  `Name`       text    NOT NULL,
  `Percentage` int(10) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Dumping data for table `criteria`
--

INSERT INTO `criteria` (`CriteriaID`, `ExposureID`, `Name`, `Percentage`) VALUES
  (7, 7, 'Intelligence', 50),
  (8, 7, 'Overall Physical Personality', 50),
  (9, 8, 'Stage Presence/Projection', 30),
  (10, 8, 'Mastery and Skill', 30),
  (11, 8, 'Suitability of Piece (Music, Costume, Props)', 20),
  (12, 8, 'Uniqueness/Creativity', 20),
  (13, 9, 'Appropriateness of Design and Accessories', 20),
  (14, 9, 'Beauty', 20),
  (15, 9, 'Poise, Projection and Stage Presence', 20),
  (16, 9, 'Confidence', 20),
  (17, 9, 'Execution', 20),
  (18, 10, 'Personal Style (Fashion sense)', 20),
  (19, 10, 'Beauty', 40),
  (20, 10, 'Confidence, poise, projection stage presence', 40),
  (21, 11, 'Body proportion', 40),
  (22, 11, 'Confidence, poise, projection and stage presence', 30),
  (23, 11, 'Beauty', 30),
  (24, 12, 'Grace, Poise, and elegance', 40),
  (25, 12, 'Confidence, projection and stage presence', 30),
  (26, 12, 'Beauty and Personal Style', 30);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `EventID` int(10) NOT NULL,
  `Name`    text    NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`EventID`, `Name`) VALUES
  (4, 'MR. & MS. BUKSU 2018');

-- --------------------------------------------------------

--
-- Table structure for table `exposures`
--

CREATE TABLE `exposures` (
  `ExposureID` int(10)    NOT NULL,
  `EventID`    int(10)    NOT NULL,
  `Name`       text       NOT NULL,
  `Percentage` int(10)    NOT NULL,
  `isDisabled` tinyint(1) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Dumping data for table `exposures`
--

INSERT INTO `exposures` (`ExposureID`, `EventID`, `Name`, `Percentage`, `isDisabled`) VALUES
  (7, 4, 'Preliminary Q and A', 30, 0),
  (8, 4, 'Talent', 10, 0),
  (9, 4, 'Ethnic Attire', 10, 0),
  (10, 4, 'Casual Attire', 10, 0),
  (11, 4, 'Playsuit', 20, 0),
  (12, 4, 'Formal Wear and Long Gown', 20, 0),
  (13, 4, 'Top 5', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `judges`
--

CREATE TABLE `judges` (
  `JudgeID`  int(10)      NOT NULL,
  `EventID`  int(10)      NOT NULL,
  `Name`     text         NOT NULL,
  `username` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Dumping data for table `judges`
--

INSERT INTO `judges` (`JudgeID`, `EventID`, `Name`, `username`, `password`) VALUES
  (1, 4, 'Ian Freitz', 'Judge1', 'Judge1'),
  (2, 4, 'Kid Romart', 'Judge2', 'Judge2'),
  (3, 4, 'Jeciril Bongolto', 'Judge3', 'Judge3'),
  (4, 4, 'Stephen John', 'Judge4', 'Judge4'),
  (5, 4, 'Deyrold Casinabe', 'Judge5', 'Judge5');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `ScoreID`     int(10) NOT NULL,
  `JudgeID`     int(10) NOT NULL,
  `CriteriaID`  int(10) NOT NULL,
  `CandidateID` int(10) NOT NULL,
  `Score`       int(10) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`CandidateID`);

--
-- Indexes for table `criteria`
--
ALTER TABLE `criteria`
  ADD PRIMARY KEY (`CriteriaID`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`EventID`);

--
-- Indexes for table `exposures`
--
ALTER TABLE `exposures`
  ADD PRIMARY KEY (`ExposureID`);

--
-- Indexes for table `judges`
--
ALTER TABLE `judges`
  ADD PRIMARY KEY (`JudgeID`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`ScoreID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `CandidateID` int(10) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 19;
--
-- AUTO_INCREMENT for table `criteria`
--
ALTER TABLE `criteria`
  MODIFY `CriteriaID` int(10) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 27;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `EventID` int(10) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 5;
--
-- AUTO_INCREMENT for table `exposures`
--
ALTER TABLE `exposures`
  MODIFY `ExposureID` int(10) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 14;
--
-- AUTO_INCREMENT for table `judges`
--
ALTER TABLE `judges`
  MODIFY `JudgeID` int(10) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 6;
--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `ScoreID` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
