<?php
//Prevents Form input injections
function input_format($data)
{
    $invalid_char = array(" or ", " OR ", "=", " UNION ", " union ",
        "'", "\\", "\"", "(", ")", "*", "SELECT ", " select ",
        "INSERT INTO ", "insert into ", "\"\"", "DROP ",
        " TABLE ", "drop ", " table ", ";", "count()", " FROM ", " from ", " set ",
        " SET ");
    foreach ($invalid_char as $val) {
        $data = str_replace($val, "", $data);
    }
    return trim(stripslashes(htmlspecialchars($data)));
}

//Encryption of Data
function encrypt($data)
{
    return base64_encode(bin2hex(hex2bin(strrev(bin2hex($data)))));
}

//Decryption of Data
function decrypt($data)
{
    return hex2bin(strrev(bin2hex(hex2bin(base64_decode($data)))));
} ?>
