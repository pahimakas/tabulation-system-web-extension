<?php
$temp_key = 2798;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['admin_request'])) {
        $pass_key = $_POST['pin'];
        if ($pass_key == $temp_key && $pass_key . length == $temp_key . length) {
            http_response_code(202);
        } else {
            http_response_code(203);
        }
    } elseif (isset($_POST['integrity'])) {
        session_start();
        include '../services/cryptic.php';
        $username = $_POST['username'];
        $hash = decrypt($_POST['hash']);
        if ($username == $hash && $username == $_SESSION['username'] && $_POST['hash'] == $_SESSION['u_en'] && $username && $_SESSION['username']) {
            http_response_code(202);
        } else {
            http_response_code(203);
        }
    } else {
        http_response_code(405);
    }
} else {
    if (isset($_GET['check_availability'])) {
        include '../shared/connect.php';
        include '../shared/filter.php';
        $sql = 'SELECT auth_user.username FROM auth_user WHERE username like \'' . alphanumeric_filter($_GET['check_availability'], false) . '\';';
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            http_response_code(203);
        } else {
            http_response_code(200);
        }
        $connection->close();
    } else {
        http_response_code(405);
    }
}

?>
