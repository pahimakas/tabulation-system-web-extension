<?php
include '../shared/connect.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['submission'])) {
        switch ($_POST['submission']) {
            case 'submit_score':
                session_start();
                if (check_connection($connection)) {
                    $data = $_POST['obj'];
                    $judgeID = $_SESSION['judge_id'];
                    $candidateID = $data['candidate_id'];
                    $criteria_list = $data['criteria_list'];
                    $is_scored = $data['is_scored'];
                    $sql = "";
                    foreach ($criteria_list as $value) {
                        if (isset($value["data"])) {
                            if ($is_scored == "false") {
                                $sql = $sql . "INSERT INTO tabulation.scores (JudgeID, CriteriaID, CandidateID, Score) VALUES (" . $judgeID . ", " . $value['criteria_id'] . ", " . $candidateID . ", " . $value["data"] . ");";
                            } elseif ($is_scored == "true" && isset($value["score_id"])) {
                                $sql = $sql . "UPDATE scores SET Score=" . $value["data"] . " WHERE scoreID=" . $value["score_id"] . ";";
                            }
                        } else {
                            if ($is_scored == "false") {
                                $sql = $sql . "INSERT INTO tabulation.scores (JudgeID, CriteriaID, CandidateID, Score) VALUES (" . $judgeID . ", " . $value['criteria_id'] . ", " . $candidateID . ", 0);";
                            } elseif ($is_scored == "true" && isset($value["score_id"])) {
                                $sql = $sql . "UPDATE scores SET Score=0 WHERE scoreID=" . $value["score_id"] . ";";
                            }
                        }
                    }
                    if ($connection->multi_query($sql) === TRUE) {
                        $id = int_filter($_POST['exposure_id']);
                        $candidate_id = int_filter($_POST['candidate_id']);
                        $sql = 'SELECT 
                    criteria.CriteriaID as CriteriaID, 
                    criteria.ExposureID as ExposureID, 
                    criteria.Name as Name, 
                    criteria.Percentage as Percentage, 
                    scores.Score as score ,
                    scores.ScoreID as score_id
                    FROM criteria 
                    INNER JOIN scores ON scores.CriteriaID = criteria.CriteriaID 
                    WHERE CandidateID = ' . $candidate_id . ' AND ExposureID = ' . $id . ' AND JudgeID = ' . $_SESSION['judge_id'] . ';';
                        $result = $connection->query($sql);
                        $data = array();
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                array_push($data,
                                    [
                                        "criteria_id" => $row['CriteriaID'],
                                        "exposure_id" => $row['ExposureID'],
                                        "name" => $row['Name'],
                                        "data" => (int)$row['score'],
                                        "score_id" => (int)$row["score_id"],
                                        "percentage" => $row['Percentage']
                                    ]);
                            }
                        }
                        http_response_code(200);
                        header('Content-type: application/json');
                        echo json_encode($data);
                    } else {
                        echo "Error updating record: " . $connection->error;
                        http_response_code(203);
                    }
                    $connection->close();
                }
                break;
            default:
                http_response_code(405);
        }
    } else {
        http_response_code(405);
    }
} else {
    if (isset($_GET['provider'])) {
        switch ($_GET['provider']) {
            case 'welcome':
                $msg = ['Hi!'];
                header('Content-type: application/json');
                echo json_encode($msg);
                break;
            case 'user_data':
                session_start();
                if (check_connection($connection)) {
                    $sql = 'SELECT * FROM judges WHERE judges.username like \'' . $_SESSION['username'] . '\';';
                    $result = $connection->query($sql);
                    $msg = '';
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            $msg = array("username" => $row['username'], "full_name" => $row['Name'], "id" => $row['JudgeID']);
                        }
                    }
                    header('Content-type: application/json');
                    echo json_encode($msg);
                }
                $connection->close();
                break;
            case 'about':
                echo "Hello";
                break;
            case 'exposure':
                if (check_connection($connection)) {
                    session_start();
                    $sql = 'SELECT * FROM tabulation.exposures WHERE EventID =' . $_SESSION["event_id"] . ';';
                    $result = $connection->query($sql);
                    $data = array();
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            array_push($data,
                                [
                                    "exposure_id" => $row['ExposureID'],
                                    "event_id" => $row['EventID'],
                                    "name" => $row['Name'],
                                    "src" => $row['ExposureID'] . "_" . $row['EventID'],
                                    "is_disabled" => $row['isDisabled'],
                                    "percentage" => $row['Percentage']
                                ]);
                        }
                    }
                    http_response_code(202);
                    header('Content-type: application/json');
                    echo json_encode($data);
                } else {
                    http_response_code(500);
                }
                $connection->close();
                break;
            case 'candidates':
                if (check_connection($connection)) {
                    session_start();
                    $exposure_id = $_GET['exposure_id'];
                    $sql = 'SELECT * FROM tabulation.candidates WHERE EventID =' . $_SESSION["event_id"] . ';';
                    $result = $connection->query($sql);
                    $male = array();
                    $female = array();
                    $data = array();
                    if ($result->num_rows > 0) {
                        $has_scored = false;
                        while ($row = $result->fetch_assoc()) {
                            $sql1 = 'SELECT DISTINCT exposures.ExposureID FROM scores right join criteria ON scores.CriteriaID = criteria.CriteriaID right join exposures ON criteria.ExposureID = exposures.ExposureID WHERE criteria.ExposureID = ' . $exposure_id . ' AND scores.CandidateID = ' . $row['CandidateID'] . ' AND scores.JudgeID = ' . $_SESSION['judge_id'] . ';';
                            $result1 = $connection->query($sql1);
                            if ($result1->num_rows > 0) {
                                $has_scored = true;
                            } else {
                                $has_scored = false;
                            }
                            $gender = $row['Gender'];
                            if ($gender == 'Female') {
                                array_push($female,
                                    [
                                        "candidate_id" => $row['CandidateID'],
                                        "event_id" => $row['EventID'],
                                        "candidate_number" => $row['CandidateNumber'],
                                        "last_name" => $row['Lastname'],
                                        "first_name" => $row['Firstname'],
                                        "project_name" => $row['Projectname'],
                                        "gender" => $row['Gender'],
                                        "active" => $row['Active'],
                                        "is_scored" => $has_scored,
                                    ]
                                );
                            } elseif ($gender == 'Male') {
                                array_push($male,
                                    [
                                        "candidate_id" => $row['CandidateID'],
                                        "event_id" => $row['EventID'],
                                        "candidate_number" => $row['CandidateNumber'],
                                        "last_name" => $row['Lastname'],
                                        "first_name" => $row['Firstname'],
                                        "project_name" => $row['Projectname'],
                                        "gender" => $row['Gender'],
                                        "active" => $row['Active'],
                                        "is_scored" => $has_scored
                                    ]
                                );
                            }
                        }
                    }
                    $data = array_merge(array("male" => $male), array("female" => $female));
                    http_response_code(202);
                    header('Content-type: application/json');
                    echo json_encode($data);
                }
                $connection->close();
                break;
            case 'criteria':
                include '../shared/filter.php';
                if (check_connection($connection) && isset($_GET['exposure_id'])) {
                    $id = int_filter($_GET['exposure_id']);
                    $sql = 'SELECT * FROM tabulation.criteria where ExposureID = ' . $id;
                    $result = $connection->query($sql);
                    $data = array();
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            array_push($data,
                                [
                                    "criteria_id" => $row['CriteriaID'],
                                    "exposure_id" => $row['ExposureID'],
                                    "name" => $row['Name'],
                                    "percentage" => $row['Percentage']
                                ]);
                        }
                    }
                    http_response_code(202);
                    header('Content-type: application/json');
                    echo json_encode($data);
                } else {
                    http_response_code(500);
                }
                $connection->close();
                break;
            case 'criteria_mod':
                include '../shared/filter.php';
                session_start();
                if (check_connection($connection) && isset($_GET['exposure_id'])) {
                    $id = int_filter($_GET['exposure_id']);
                    $candidate_id = int_filter($_GET['candidate_id']);
                    $sql = 'SELECT 
                    criteria.CriteriaID as CriteriaID, 
                    criteria.ExposureID as ExposureID, 
                    criteria.Name as Name, 
                    criteria.Percentage as Percentage, 
                    scores.Score as score ,
                    scores.ScoreID as score_id
                    FROM criteria 
                    INNER JOIN scores ON scores.CriteriaID = criteria.CriteriaID 
                    WHERE CandidateID = ' . $candidate_id . ' AND ExposureID = ' . $id . ' AND JudgeID = ' . $_SESSION['judge_id'] . ';';
                    $result = $connection->query($sql);
                    $data = array();
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            array_push($data,
                                [
                                    "criteria_id" => $row['CriteriaID'],
                                    "exposure_id" => $row['ExposureID'],
                                    "name" => $row['Name'],
                                    "data" => (int)$row['score'],
                                    "score_id" => (int)$row["score_id"],
                                    "percentage" => $row['Percentage']
                                ]);
                        }
                        http_response_code(202);
                        header('Content-type: application/json');
                        echo json_encode($data);
                    }
                } else {
                    http_response_code(500);
                }
                $connection->close();
                break;
            case 'events':
                if (check_connection($connection)) {
                    $sql = 'SELECT * FROM tabulation.events';
                    $result = $connection->query($sql);
                    $data = array();
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            array_push($data,
                                [
                                    "event_id" => $row["EventID"],
                                    "name" => $row["Name"]
                                ]
                            );
                        }
                    }
                    http_response_code(202);
                    header('Content-type: application/json');
                    echo json_encode($data);
                } else {
                    http_response_code(500);
                }
                $connection->close();
                break;
            case 'event_info':
                if (check_connection($connection)) {
                    session_start();
                    $sql = 'SELECT * FROM tabulation.events WHERE EventID=' . $_SESSION['event_id'];
                    $result = $connection->query($sql);
                    $data = array();
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            array_push($data,
                                [
                                    "event_id" => $row["EventID"],
                                    "name" => $row["Name"]
                                ]
                            );
                        }
                    }
                    http_response_code(202);
                    header('Content-type: application/json');
                    echo json_encode($data[0]);
                } else {
                    http_response_code(500);
                }
                $connection->close();
                break;
            default:
                $connection->close();
                break;
        }
    }
}

?>
