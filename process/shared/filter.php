<?php

function char_filter($txt, $allow_whitespace)
{
    $pattern = $allow_whitespace ? '/[^a-zA-Z0-9\s]/u' : '/[^a-zA-Z0-9]/u';
    return preg_replace($pattern, '', (string)$txt);
}

function alphanumeric_filter($txt, $allow_whitespace)
{
    $pattern = $allow_whitespace ? '/[^A-Za-z0-9\s ]/' : '/[^A-Za-z0-9 ]/';
    return preg_replace($pattern, '', (string)$txt);
}

function int_filter($txt)
{
    return preg_replace('/[^0-9]/s', '', (string)$txt);
}