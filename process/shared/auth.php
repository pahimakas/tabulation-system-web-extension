<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['action'])) {
        switch ($_POST['action']) {
            case 'login':
                include '../shared/connect.php';
                include '../shared/filter.php';
                include '../services/cryptic.php';
                if (check_connection($connection)) {
                    $username = alphanumeric_filter($_POST['username'], false);
                    $password = $_POST['password'];
                    $event_id = $_POST['event_id'];
                    $sql = "SELECT judges.JudgeID, judges.username, judges.password, judges.EventID FROM judges where username like '$username' and password like '$password'";
                    $result = $connection->query($sql);
                    if ($result->num_rows == 1) {
                        while ($row = $result->fetch_assoc()) {
                            if ($row['EventID'] == $event_id) {
                                session_start();
                                $_SESSION["event_id"] = $event_id;
                                $_SESSION["judge_id"] = $row['JudgeID'];
                                $_SESSION["u_en"] = encrypt($username);
                                $_SESSION["username"] = $username;
                                $data = ["shake" => ["hash" => $_SESSION["u_en"], "username" => $_SESSION["username"]]];
                                echo json_encode($data);
                                http_response_code(202);
                                break;
                            } else {
                                http_response_code(201);
                                break;
                            }
                        }
                    } else {
                        http_response_code(203);
                    }
                } else {
                    http_response_code(500);
                }
                break;
            default:
                http_response_code(405);
        }
    } else {
        http_response_code(405);
    }
} else {
    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            case 'logout':
                session_start();
                session_unset();
                session_destroy();
                http_response_code(202);
                break;
            default:
                http_response_code(405);
                break;
        }
    } else {
        http_response_code(405);
    }
}
?>
