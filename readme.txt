Web Based Tabulation System Extension [Pahimakas]

System Requirements
    - XAMP Server
    - MySQL Database Server

System Deployment Instruction
    Create new folder on XAMP htdocs directory
    Put the following files into newly created directory
        [media, process, static. index.html]
    candidate.zip naa sa gc
    Extract [candidates.zip] to 'media/' directory
    Run XAMP [Apache, MySQL]
    Export Demo tabulation database on PHPMyAdmin with your preferred credential
    Edit 'connect.php' with your MySQL Credential and save
        -File Path: process/shared/connect.php
    Goto http://127.0.0.1/{name of the directory you created}

Demo Tabulation Data (Default Credentials)
    |Judge Name: Ian Freitz
    |username: Judge1
    |password: Judge1
    |-------------------------
    |Judge Name: Kid Romart
    |username: Judge2
    |password: Judge2
    |-------------------------
    |Judge Name: Jeciril Bongolto
    |username: Judge3
    |password: Judge3
    |-------------------------
    |Judge Name: Stephen John
    |username: Judge4
    |password: Judge4
    |-------------------------
    |Judge Name: Deyrold Casinabe
    |username: Judge5
    |password: Judge5

